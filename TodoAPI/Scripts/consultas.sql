IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Selecionar]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[Selecionar]
GO

CREATE PROCEDURE [dbo].[Selecionar]
		
	AS

	/*
	Documentaçao
	Arquivo Fonte.....: TodoList.sql
	Objetivo..........:  Pega todos as Tarefas
	Autor.............: Tiago Douglas	
	Data..............: 26/05/2017
	Ex................: EXEC [dbo].[Selecionar]
	*/

	BEGIN

		SELECT	IdTarefa,
				Nome, 
				DataInicio, 
				DataTermino, 
				Imagem 
			FROM Tarefas WITH(NOLOCK)

	END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Buscar]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[Buscar]
GO

CREATE PROCEDURE [dbo].[Buscar]
	@IdTarefa			INT
		
	AS

	/*
	Documentaçao
	Arquivo Fonte.....: TodoList.sql
	Objetivo..........:  Buscar tarefa por Id
	Autor.............: Tiago Douglas	
	Data..............: 20/06/2017
	Ex................: EXEC [dbo].[Buscar]26
	*/

	BEGIN

		SELECT	IdTarefa, 
				Nome, 
				DataInicio, 
				DataTermino, 
				Imagem 
			FROM Tarefas WITH(NOLOCK)
		WHERE IdTarefa = @IdTarefa

	END
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Inserir]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[Inserir]
GO

CREATE PROCEDURE [dbo].[Inserir]
	@Nome				VARCHAR(200),
	@DataInicio			DATETIME,
	@DataTermino		DATETIME = NULL,
	@Imagem				VARCHAR(200) = NULL,
	@RETURN_VALUE		INT OUT
	
	AS

	/*
	Documentaçao
	Arquivo Fonte.....: TodoList.sql
	Objetivo..........: Insere uma nova tarefa
	Autor.............: Tiago Douglas	
	Data..............: 26/05/2017
	Ex................: EXEC [dbo].[Inserir]'Teste', '2017-05-24',null,null, NULL
	*/

	BEGIN

		INSERT INTO	Tarefas	
		(
			Nome, 
			DataInicio, 
			DataTermino, 
			Imagem
		) 
		VALUES 
		(
			@Nome, 
			@DataInicio, 
			@DataTermino, 
			@Imagem
		)
		
		SELECT SCOPE_IDENTITY() AS IdTarefa
		

	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Atualizar]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[Atualizar]
GO

CREATE PROCEDURE [dbo].[Atualizar]
	@IdTarefa			INT,
	@Nome				VARCHAR(200) = NULL,
	@DataInicio			DATETIME = NULL,
	@DataTermino		DATETIME = NULL,
	@Imagem				VARCHAR(200) = NULL

	AS

	/*
	Documentaçao
	Arquivo Fonte.....: TodoList.sql
	Objetivo..........:  Atualiza uma tarefa
	Autor.............: Tiago Douglas	
	Data..............: 26/05/2017
	Ex................: EXEC [dbo].[Atualizar]
	*/

	BEGIN

		UPDATE Tarefas SET  
			Nome		= CASE WHEN @Nome IS NOT NULL THEN @Nome ELSE Nome END, 
			DataInicio	= CASE WHEN @DataInicio IS NOT NULL THEN @DataInicio ELSE DataInicio END, 
			DataTermino = CASE WHEN @DataTermino IS NOT NULL THEN @DataTermino ELSE NULL END, 
			Imagem		= CASE WHEN @Imagem IS NOT NULL THEN @Imagem ELSE Imagem END	
		WHERE IdTarefa	= @IdTarefa
		 
	END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Deletar]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[Deletar]
GO

CREATE PROCEDURE [dbo].[Deletar]
	@IdTarefa			INT	

	AS

	/*
	Documentaçao
	Arquivo Fonte.....: TodoList.sql
	Objetivo..........:  Deleta uma tarefa
	Autor.............: Tiago Douglas	
	Data..............: 26/05/2017
	Ex................: EXEC [dbo].[Deletar]
	*/

	BEGIN
	
		DELETE FROM Tarefas 
			WHERE IdTarefa = @IdTarefa
		
	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[Concluir]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[Concluir]
GO

CREATE PROCEDURE [dbo].[Concluir]
	@IdTarefa			INT,
	@DataTermino		DATETIME = NULL	

	AS

	/*
	Documentaçao
	Arquivo Fonte.....: TodoList.sql
	Objetivo..........:  Conclui, desconclui uma tarefa
	Autor.............: Tiago Douglas	
	Data..............: 26/05/2017
	Ex................: EXEC [dbo].[Concluir]5,null
	*/

	BEGIN
		
		UPDATE Tarefas SET 
			DataTermino = @DataTermino
		WHERE IdTarefa = @IdTarefa
		
	END
GO


