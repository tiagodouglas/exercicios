﻿using System;
using System.Collections.Generic;
using ToDo.Core.ToDoList;

namespace ToDo.Data
{
    public class ToDoListRepository : IToDoListRepository
    {
        public enum Procedure
        {
            Selecionar,
            Buscar,
            Inserir,
            Atualizar,
            Concluir,
            Deletar
        }

        
        private ToDoProcedures _toDoProcedures;


        #region Get
        public List<TodoItem> GetAll()
        {
            var lista = new List<TodoItem>();
            using (_toDoProcedures = new ToDoProcedures())
            {
                var dataReader = _toDoProcedures.Execute(Procedure.Selecionar.ToString());
                while (dataReader.Read())
                {
                    lista.Add(new TodoItem(
                        dataReader.ReadAsInt("IdTarefa"),
                        dataReader.ReadAsString("Nome"),
                        dataReader.ReadAsDateTime("DataInicio"),
                        dataReader.ReadAsDateTimeNull("DataTermino"),
                        dataReader.ReadAsString("Imagem")
                    ));
                }
            }
            return lista;
        }

        public TodoItem GetById(int id)
        {
          
            using (_toDoProcedures = new ToDoProcedures())
            {
                var dataReader = _toDoProcedures.Execute(
                    Procedure.Buscar.ToString(), new object[] { "@IdTarefa" }, new object[] { id });

                if (dataReader.Read())
                    return new TodoItem(
                        dataReader.ReadAsInt("IdTarefa"),
                        dataReader.ReadAsString("Nome"),
                        dataReader.ReadAsDateTime("DataInicio"),
                        dataReader.ReadAsDateTimeNull("DataTermino"),
                        dataReader.ReadAsString("Imagem")
                    );
            }
            return null;
        }


        #endregion

        #region Post(Inserts) 

        public TodoItem InsertToDo(TodoItem todoItem)
        {
            using (_toDoProcedures = new ToDoProcedures())
            {
                var dataReader = _toDoProcedures.Execute(
                     Procedure.Inserir.ToString(), new object[]
                     {
                         "@Nome",
                         "@DataInicio",
                         "@DataTermino",
                         "@Imagem"
                     },
                     new object[] {
                         todoItem.Nome,
                         todoItem.DataInicio,
                         todoItem.DataTermino,
                         todoItem.Imagem
                     }, true);

                todoItem.IdTarefa = Convert.ToInt32(dataReader);
                return todoItem;
            }
        }
        #endregion

        #region Put (Updates)

        public void UpdateToDo(TodoItem todoItem)
        {
            using (_toDoProcedures = new ToDoProcedures())
            {
                _toDoProcedures.ExecuteQuery(
                    Procedure.Atualizar.ToString(), new object[]
                    {
                        "@IdTarefa",
                        "@Nome",
                        "@DataInicio",
                        "@DataTermino",
                        "@Imagem"
                    },
                    new object[] {
                        todoItem.IdTarefa,
                        todoItem.Nome,
                        todoItem.DataInicio,
                        todoItem.DataTermino,
                        todoItem.Imagem
                    });
            }

        }

        public void FinishToDo(TodoItem todoItem)
        {
            using (_toDoProcedures = new ToDoProcedures())
            {
                _toDoProcedures.ExecuteQuery(
                    Procedure.Concluir.ToString(), new object[]
                    {
                        "@IdTarefa",
                        "@DataTermino"
                    },
                    new object[]
                    {
                        todoItem.IdTarefa,
                        todoItem.DataTermino
                    });
            }
        }

        #endregion

        #region Delete

        public void DeleteToDo(int idTarefa)
        {
            using (_toDoProcedures = new ToDoProcedures())
            {
                _toDoProcedures.ExecuteQuery(
                    Procedure.Deletar.ToString(), new object[]
                    {
                        "@IdTarefa"
                    },
                    new object[]
                    {
                        idTarefa
                    });
            }
        }

        #endregion

    }
}
