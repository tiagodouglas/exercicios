﻿using Microsoft.EntityFrameworkCore;
using ToDo.Core.ToDoList;

namespace Todo.Data.Context
{
    public class TodoContext : DbContext
    {
        public DbSet<TodoItem> TodoItens { get; set; }

        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoItem>().HasKey(c => c.IdTarefa);
        }
    }
}
