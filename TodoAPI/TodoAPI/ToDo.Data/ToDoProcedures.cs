﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ToDo.Data
{


    public class ToDoProcedures: IDisposable
    {
        private readonly SqlConnection _connection;
        public ToDoProcedures()
        {
            try
            {
                _connection = new SqlConnection(
                    @"Server=TIAGODOUGLAS-PC\SQLEXPRESS;Database=db_TodoList;User Id=sa;Password = bk201;");
                _connection.Open();
            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Erro na conexão com a database", ex);
            }
        }

        public SqlDataReader Execute(string procedureName)
        {
            try
            {
                var consulta = new SqlCommand(procedureName, _connection) {CommandType = CommandType.StoredProcedure};
                return consulta.ExecuteReader();
            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Sintaxe inválida", ex);
            }
            
        }

        public SqlDataReader Execute(string procedureName, object[] param, object[] values)
        {
            try
            {
                var consulta = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure };

                for (var i = 0; i < param.Length; i++)
                {
                    consulta.Parameters.AddWithValue(param[i].ToString(), values[i]);
                }
            
                 var id = consulta.ExecuteScalar();

                return consulta.ExecuteReader();

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Sintaxe inválida", ex);
            }

        }

        public object Execute(string procedureName, object[] param, object[] values, bool retorno)
        {
            try
            {
                var consulta = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure };

                for (var i = 0; i < param.Length; i++)
                {
                    consulta.Parameters.AddWithValue(param[i].ToString(), values[i]);
                }

                return consulta.ExecuteScalar();

                

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Sintaxe inválida", ex);
            }

        }

        public void ExecuteQuery(string procedureName, object[] param, object[] values )
        {
            try
            {
                var consulta = new SqlCommand(procedureName, _connection) {CommandType = CommandType.StoredProcedure};
                for (var i = 0; i < param.Length; i++)
                {
                    consulta.Parameters.AddWithValue(param[i].ToString(), values[i]);
                }
                consulta.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new InvalidOperationException("Sintaxe inválida", ex);
            }
           
        }

        public void Dispose()
        {
            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
        }
    }
}
