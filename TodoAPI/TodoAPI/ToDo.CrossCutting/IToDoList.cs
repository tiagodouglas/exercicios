﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.CrossCutting
{
    public interface IToDoList
    {
        int IdTarefa { get; set; }
        string Nome { get; set; }
        DateTime? DataInicio { get; set; }
        DateTime? DataTermino { get; set; }
        string Imagem { get; set; }
    }
}
