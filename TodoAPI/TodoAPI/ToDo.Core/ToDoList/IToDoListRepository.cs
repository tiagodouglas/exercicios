﻿using System.Collections.Generic;

namespace ToDo.Core.ToDoList
{
    public interface IToDoListRepository
    {
        List<TodoItem> GetAll();
        TodoItem GetById(int id);
        TodoItem InsertToDo(TodoItem todoItem);
        void UpdateToDo(TodoItem todoItem);
        void FinishToDo(TodoItem todoItem);
        void DeleteToDo(int idTarefa);
    }
}
