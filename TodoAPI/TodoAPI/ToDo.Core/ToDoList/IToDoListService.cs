﻿using System.Collections.Generic;

namespace ToDo.Core.ToDoList
{
    public interface IToDoListService
    {
        IEnumerable<TodoItem> GetAll();

        TodoItem GetById(int id);

        void InsertTodo(TodoItem todoItem);

        void UpdateTodo(int id, TodoItem todoItem);

        void Concluir(int id);

        void RemoveConclusao(int id);

        void Delete(int id);
    }
}
