﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo.Core.ToDoList
{
    public class TodoItemDto
    {
        public string Nome { get; set; }

        public DateTime? DataInicio { get; set; }

        public DateTime? DataTermino { get; set; }

        public string Imagem { get; set; }
    }
}
