﻿using System;

namespace ToDo.Core.ToDoList
{
    public class TodoItem
    {
        public TodoItem(int idTarefa, string nome, DateTime? dataInicio, DateTime? dataTermino, string imagem)
        {
            IdTarefa = idTarefa;
            Nome = nome;
            DataInicio = dataInicio;
            DataTermino = dataTermino;
            Imagem = imagem;
        }

        public TodoItem(string nome, DateTime? dataInicio, DateTime? dataTermino, string imagem)
        {
            Nome = nome;
            DataInicio = dataInicio;
            DataTermino = dataTermino;
            Imagem = imagem;
        }

        public TodoItem(DateTime? dataTermino)
        {
            DataTermino = dataTermino;
        }

        public TodoItem(int idTarefa)
        {
            IdTarefa = idTarefa;
        }

        public TodoItem()
        {
        }

        public int IdTarefa { get; set; }
        public string Nome { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataTermino { get; set; }
        public string Imagem { get; set; }
    }
}
