﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;



namespace ToDo.Core.ToDoList
{
    public class ToDoListService : IToDoListService
    {
        private readonly IToDoListRepository _iToDoListRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ToDoListService(IToDoListRepository iToDoListRepository, IHostingEnvironment hostingEnvironment)
        {
            _iToDoListRepository = iToDoListRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        public IEnumerable<TodoItem> GetAll()
        {
            try
            {
                return _iToDoListRepository.GetAll();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao obter os dados", ex);
            }

        }

        public TodoItem GetById(int id)
        {
            try
            {
                return _iToDoListRepository.GetById(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao obter os dados", ex);
            }
        }

        public void InsertTodo(TodoItem todoItem)
        {
            try
            {
                var imagem = ConverteImagem(todoItem.Imagem);
                todoItem.Imagem = null;
               
                var retorno = _iToDoListRepository.InsertToDo(todoItem);

                SalvarImagem(imagem,retorno);
            }
            catch (Exception ex)
            {
                throw new Exception("Parametros invalidos", ex);
            }

        }

        public void UpdateTodo(int id, TodoItem todoItem)
        {
            try
            {
                var imagem = ConverteImagem(todoItem.Imagem);
               SalvarImagem(imagem,todoItem);
            }
            catch (Exception ex)
            {
                throw new Exception("Parametros invalidos", ex);
            }

        }

        public void Concluir(int id)
        {
            try
            {
                DateTime data = DateTime.Now;
                TodoItem todoItem = new TodoItem
                {
                    IdTarefa = id,
                    DataTermino = data
                };
                _iToDoListRepository.FinishToDo(todoItem);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro, parametros inválidos", ex);
            }

    }

        public void RemoveConclusao(int id)
        {
            try
            {
                TodoItem todoItem = new TodoItem
                {
                    IdTarefa = id,
                    DataTermino = null
                };
                _iToDoListRepository.FinishToDo(todoItem);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro, parametros inválidos", ex);
            }

        }

        public void Delete(int id)
        {
            try
            {
                _iToDoListRepository.DeleteToDo(id);
                var path = _hostingEnvironment.WebRootPath + "\\images";
                var arquivo = Path.Combine(path, id + ".png");
                File.Delete(arquivo);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro,  parametros inválidos", ex);
            }
        }

        private Image ConverteImagem(string img)
        {
            byte[] imageBytes = Convert.FromBase64String(img);
            MemoryStream imagemMemoryStream = new MemoryStream(imageBytes, 0, imageBytes.Length);
            imagemMemoryStream.Write(imageBytes, 0, imageBytes.Length);
            Image imagem = Image.FromStream(imagemMemoryStream, true);

            return imagem;
        }

        private void SalvarImagem(Image imagem, TodoItem todoItem)
        {
            var path = _hostingEnvironment.WebRootPath + "\\images";
            var arquivo = Path.Combine(path, todoItem.IdTarefa + ".png");

            //Atualiza a imagem no banco
            imagem.Save(arquivo, ImageFormat.Png);
            todoItem.Imagem = "\\images\\" + todoItem.IdTarefa + ".png";
            _iToDoListRepository.UpdateToDo(todoItem);

        }
    }
}
