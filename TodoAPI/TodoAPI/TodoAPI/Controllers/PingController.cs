using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ToDo.Api.Controllers
{
    
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class PingController : Controller
    {

        [HttpGet]
        public string Get()
        {
            return $"{DateTime.Now:dd/MM/yyyy HH:mm:ss}";
        }
    }
}