﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Todo.Data.Context;
using ToDo.Core.ToDoList;


namespace Todo.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ToDoController : Controller
    {
        private readonly IToDoListService _toDoListService;

        public ToDoController(IToDoListService toDoListService)
        {
            _toDoListService = toDoListService;
        }

        [HttpGet]
        public IEnumerable<TodoItem> Get()
        {
            var items = _toDoListService.GetAll();
            if (items == null)
            {
                HttpContext.Response.StatusCode = 500;
            }
            return items;
        }

        [HttpGet("{id}", Name = "GetTodo")]
        public IActionResult Get([FromServices]TodoContext context, int id)
        {

            var item = _toDoListService.GetById(id);
            if (item == null)
                NotFound();

            return new ObjectResult(item);
        }

        [HttpPost]
        public void Post([FromBody]TodoItem todoItem)
        {
            if (todoItem == null)
                HttpContext.Response.StatusCode = 400;
            else
            {
                _toDoListService.InsertTodo(todoItem);
                HttpContext.Response.StatusCode = 200;
            }
                
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]TodoItem todoItem)
        {
            if(todoItem != null)
                _toDoListService.UpdateTodo(id, todoItem);
           else
                HttpContext.Response.StatusCode = 400;
            
        }
        
        [HttpPut("{id}/Concluir")]
        public void Concluir(int id)
        {
          _toDoListService.Concluir(id);
        }


        [HttpPut("{id}/Remove")]
        public void Remove(int id)
        {
           _toDoListService.RemoveConclusao(id);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
           _toDoListService.Delete(id);
        }

    }
}
