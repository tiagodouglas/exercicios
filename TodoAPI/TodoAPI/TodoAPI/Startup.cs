﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Todo.Data.Context;
using ToDo.Api.Middleware;
using ToDo.Api.Models;
using ToDo.Core.ToDoList;
using ToDo.Data;


namespace ToDo.Api
{
    public class Startup
    {
    

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer()
                    .AddDbContext<TodoContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TodoList")));
            services.AddScoped<IToDoListRepository, ToDoListRepository>();
            services.AddScoped<IToDoListService, ToDoListService>();
            services.AddMvc();
        }

        private static readonly string secretKey = "meusupertoken!123@123*";
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            app.UseTokenProvider(new TokenProviderOptions
            {
                Path = "/api/login",
                Audience = "ExemploAudience",
                Issuer = "ExemploIssuer",
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,
                ValidIssuer = "ExemploIssuer",
                ValidateAudience = true,
                ValidAudience = "ExemploAudience",
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });

            
            app.UseMiddleware<CustomMiddleware>();
            app.UseExceptionHandler();
            app.UseStaticFiles();
            app.UseMvc();
        }
       
        private Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            
            if (username == "admin" && password == "1234")
            {
                return Task.FromResult(new ClaimsIdentity(new GenericIdentity(username, "Token"), new Claim[] { }));
            }
            return Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
