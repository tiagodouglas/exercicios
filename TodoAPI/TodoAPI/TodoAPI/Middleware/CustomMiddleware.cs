﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;


namespace ToDo.Api.Middleware
{
    public sealed class CustomMiddleware  
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        

        public CustomMiddleware(RequestDelegate next, ILoggerFactory logger)
        {
            _next = next;
            _logger = logger.CreateLogger<CustomMiddleware>();

        }

        public async Task Invoke(HttpContext context)
        {
            await _next(context);
            if (context.Response.StatusCode == 404)
            {
                _logger.LogTrace("Erro 404: {httpContext.Request.Path}");
                var retorno = new
                {
                    erro = "Erro 404",
                    message = "Não encontrado"
                };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(retorno));

            }
           
            if (context.Response.StatusCode == 400)
            {
                _logger.LogTrace("Erro 400: {httpContext.Request.Path}");
                var retorno = new
                {
                    erro = "Erro 400",
                    message = "O pedido não pode ser entregue devido à sintaxe incorreta."
                };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(retorno));

            }
            if (context.Response.StatusCode == 406)
            {
                _logger.LogTrace("Erro 406: {httpContext.Request.Path}");
                var retorno = new
                {
                    erro = "Erro 406",
                    message = "Falha na api"
                };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(retorno));

            }

            if (context.Response.StatusCode == 500)
            {
                _logger.LogTrace("Erro 500: {httpContext.Request.Path}");
                var retorno = new
                {
                    erro = "Erro 500",
                    message = "Erro interno"
                };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(retorno));

            }

            if (context.Response.StatusCode == 401)
            {
                _logger.LogTrace("Erro 401: {httpContext.Request.Path}");
                var retorno = new
                {
                    erro = "Erro 401",
                    message = "Você nao tem permissões"
                };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(retorno));

            }

         

            
        }
    }

    public static class CustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomMiddleware>();
        }

        public static IServiceCollection AddMiddlewareInMemory(this IServiceCollection services)
        {
            return services.AddSingleton<CustomMiddleware>();
        }
    }
}
