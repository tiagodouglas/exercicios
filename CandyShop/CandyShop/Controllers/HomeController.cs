﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CandyShop.Models;

namespace CandyShop.Controllers
{
    public class HomeController : Controller
    {
        #region CRUD
        public ActionResult Index(int? id)
        {
            var app = new Aplicacao();
            var retorno = new CandyShopViewModel();
            var arquivo = Server.MapPath("~/Content/img/Produtos/temp.jpg");
            if (System.IO.File.Exists(arquivo))
            {
                System.IO.File.Delete(arquivo);
            }
            retorno.ComboTipoProduto = new SelectList(app.ListarTipos(id), "Id", "Nome");
            return View("Index", retorno);
        }

        public ActionResult VisualizarProdutos(int id)
        {
            var app = new Aplicacao();
            var retorno = app.BuscarProdutosPorId(id);
            return View("_Produto", retorno.First());
        }

        public ActionResult EditarProdutos(int id)
        {
            var app = new Aplicacao();
            var retorno = app.BuscarProdutosPorId(id);
            return View("_Produto", retorno.First());
        }

        public void AtualizaQtde(int? id, int? qtd)
        {
            var app = new Aplicacao();
            app.AtualizaQuantidade(id, qtd);
        }

        public void SetSalvar(int idProduto, int idMarca, int idTipo, int novoMarca, int novoTipo,
            string nomeProduto, int qtd)
        {
            var app = new Aplicacao();
            app.AlteraProduto(idProduto, idMarca, idTipo, novoMarca, novoTipo, nomeProduto, qtd);
            var arquivo = Server.MapPath("~/Content/img/Produtos/temp.jpg");
            if (System.IO.File.Exists(arquivo))
            {
                System.IO.File.Copy(arquivo, Server.MapPath("~/") + "Content/img/Produtos/" + idProduto + ".jpg", true);
                System.IO.File.Delete(arquivo);
            }
        }

        public void ExcluirProduto(int idProduto)
        {
            var app = new Aplicacao();
            var arquivo = Server.MapPath("~/Content/img/Produtos/" + idProduto + ".jpg");
            app.Excluir(idProduto);
            if (System.IO.File.Exists(arquivo))
            {
                System.IO.File.Delete(arquivo);
            }
        }

        public void CadastrarNovoProduto(string nomeProduto, string novoT, string novoM, int idTipoProduto,
            int idMarca, decimal valorCompra, decimal valorVenda, int quantidadeEstoque)
        {
            var app = new Aplicacao();
            if (idTipoProduto != -1 && idMarca != -1)
            {

                app.CadastraProdutosComMarcaTipoExistentes(nomeProduto.ToUpper(), idTipoProduto, idMarca, valorCompra,
                    valorVenda, quantidadeEstoque);
                var imagem = app.GetLastProduto().First().IdProduto + ".jpg";
                app.CadastraImagem(imagem);
                var arquivo = Server.MapPath("~/Content/img/Produtos/temp.jpg");
                var arquivoDefault = Server.MapPath("~/Content/img/Produtos/product.jpg");
                if (System.IO.File.Exists(arquivo))
                {
                    System.IO.File.Copy(arquivo, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);
                    System.IO.File.Delete(arquivo);
                }
                else
                {
                    System.IO.File.Copy(arquivoDefault, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);
                }

            }
            else if (idTipoProduto == -1 && idMarca == -1)
            {
                app.CadastraMarca(novoM.ToUpper());
                app.CadastraTipo(novoT);
                var lastTipo = app.GetLastTipo().First().Id;
                var lastMarca = app.GetLastMarca().First().Id;
                app.CadastraProdutosComMarcaTipoExistentes(nomeProduto.ToUpper(), (int)lastTipo, (int)lastMarca, valorCompra,
                    valorVenda, quantidadeEstoque);
                var imagem = app.GetLastProduto().First().IdProduto + ".jpg";
                app.CadastraImagem(imagem);
                var arquivo = Server.MapPath("~/Content/img/Produtos/temp.jpg");
                var arquivoDefault = Server.MapPath("~/Content/img/Produtos/product.jpg");
                if (System.IO.File.Exists(arquivo))
                {
                    System.IO.File.Copy(arquivo, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);
                    System.IO.File.Delete(arquivo);
                }
                else
                {
                    System.IO.File.Copy(arquivoDefault, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);

                }

            }
            else if (idTipoProduto == -1 && idMarca != -1)
            {
                app.CadastraTipo(novoT);
                var lastTipo = app.GetLastTipo().First().Id;
                app.CadastraProdutosComMarcaTipoExistentes(nomeProduto.ToUpper(), (int)lastTipo, idMarca, valorCompra,
                    valorVenda, quantidadeEstoque);
                var imagem = app.GetLastProduto().First().IdProduto + ".jpg";
                app.CadastraImagem(imagem);
                var arquivo = Server.MapPath("~/Content/img/Produtos/temp.jpg");
                var arquivoDefault = Server.MapPath("~/Content/img/Produtos/product.jpg");

                if (System.IO.File.Exists(arquivo))
                {
                    System.IO.File.Copy(arquivo, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);
                    System.IO.File.Delete(arquivo);
                }
                else
                {
                    System.IO.File.Copy(arquivoDefault, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);

                }

            }
            else if (idTipoProduto != -1 && idMarca == -1)
            {
                app.CadastraMarca(novoM.ToUpper());
                var lastMarca = app.GetLastMarca().First().Id;
                app.CadastraProdutosComMarcaTipoExistentes(nomeProduto.ToUpper(), idTipoProduto, (int)lastMarca, valorCompra,
                    valorVenda, quantidadeEstoque);
                var imagem = app.GetLastProduto().First().IdProduto + ".jpg";
                app.CadastraImagem(imagem);
                var arquivo = Server.MapPath("~/Content/img/Produtos/temp.jpg");
                var arquivoDefault = Server.MapPath("~/Content/img/Produtos/product.jpg");

                if (System.IO.File.Exists(arquivo))
                {
                    System.IO.File.Copy(arquivo, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);
                    System.IO.File.Delete(arquivo);
                }
                else
                {
                    System.IO.File.Copy(arquivoDefault, Server.MapPath("~/") + "Content/img/Produtos/" + imagem, true);

                }
            }
        }
        #endregion

        #region Get informações do produto
        public ActionResult GetGrid(string nomeProduto, int? idComboTipoProduto, int? idComboMarca,
            decimal? faixaPrecoMin, decimal? faixaPrecoMax)
        {
            var app = new Aplicacao();
            var retorno = app.BuscarProdutos(nomeProduto, idComboTipoProduto, idComboMarca, faixaPrecoMin, faixaPrecoMax);
            return View("_grid", retorno);
        }

        public ActionResult GetTipos(int? id)
        {
            try
            {
                var app = new Aplicacao();
                var retorno = new CandyShopViewModel();
                retorno.ComboTipoProduto = new SelectList(app.ListarTipos(id), "Id", "Nome", "MarcaProduto.Id", false);
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.DenyGet);

            }

        }
        public ActionResult CadastroProdutos()
        {
            return View("_CadastroProdutos");
        }

        public ActionResult GetMarca(int? idTipoProduto)
        {
            try
            {
                var app = new Aplicacao();
                var retorno = new SelectList(app.ListarMarca(idTipoProduto), "Id", "Nome");
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult GetProdutos()
        {
            try
            {
                var app = new Aplicacao();
                var retorno = Json(app.ListarProdutos(), JsonRequestBehavior.AllowGet);
                return retorno;
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.DenyGet);
            }
        }
        #endregion

        #region Manipula imagens do produto
        public string GetImagem(int id)
        {
            var app = new Aplicacao();
            var imagem = app.BuscaImagem(id);
            return imagem.First().Img;
        }

        public ActionResult GetImagemUpload(string nome)
        {
            bool salvo = true;
            string fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        var diretorio = new DirectoryInfo(string.Format("{0}Content\\img", Server.MapPath(@"\")));
                        string pathString = Path.Combine(diretorio.ToString(), "Produtos");
                        bool isExists = Directory.Exists(pathString);
                        if (!isExists)
                            Directory.CreateDirectory(pathString);

                       var caminho = string.Format("{0}\\{1}", pathString, "temp.jpg"); //file.FileName
                        file.SaveAs(caminho);
                    }

                }

            }
            catch (Exception ex)
            {
                salvo = false;
            }

            if (salvo)
                return Json(new { Message = fName });

            return Json(new { Message = "Erro ao salvar o arquivo" });
        }
        #endregion
    }
}