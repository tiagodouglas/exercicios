﻿namespace CandyShop
{
    public class MarcaProduto
    {
        public MarcaProduto(int? id, string nome)
        {
            Id = id;
            Nome = nome;
        }
        public int? Id { get; set; }
        public string Nome { get; set; }
    }
}