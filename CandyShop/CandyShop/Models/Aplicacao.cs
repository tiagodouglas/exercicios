﻿using CandyShop.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CandyShop
{
    public class Aplicacao
    {
        #region Cadastros
        public void CadastraProdutosComMarcaTipoExistentes(string nomeProduto, int idTipoProduto,
            int idMarca, decimal valorCompra, decimal valorVenda, int quantidadeEstoque)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("CadastraProdutoComMarcaTipoExistentes", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@NomeProduto", nomeProduto);
            consulta.Parameters.AddWithValue("@IdTipoProduto", idTipoProduto);
            consulta.Parameters.AddWithValue("@IdMarca", idMarca);
            consulta.Parameters.AddWithValue("@ValorCompra", valorCompra);
            consulta.Parameters.AddWithValue("@ValorVenda", valorVenda);
            consulta.Parameters.AddWithValue("@QuantidadeEstoque", quantidadeEstoque);
            consulta.ExecuteNonQuery();
            minhaconexao.Close();

        }

        public void CadastraMarca(string nome)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("CadastraMarca", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@NomeMarca", nome);
            consulta.ExecuteNonQuery();
            minhaconexao.Close();

        }

        public void CadastraTipo(string nome)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("CadastraTipo", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@NomeTipoProduto", nome);
            consulta.ExecuteNonQuery();
            minhaconexao.Close();
        }

        public void CadastraImagem(string nome)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("CadastraImagem", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@Imagem", nome);

            consulta.ExecuteNonQuery();
            minhaconexao.Close();

        }
        #endregion

        #region Alterações no produto
        public void AtualizaQuantidade(int? id, int? qtd)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();

            var consulta = new SqlCommand("AtualizaQuantidadeEstoque", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@idProduto", id);
            consulta.Parameters.AddWithValue("@qtd", qtd);

            consulta.ExecuteNonQuery();
            minhaconexao.Close();


        }

        public void Excluir(int idProduto)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("ExcluirProduto", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@IdProduto", idProduto);
            consulta.ExecuteNonQuery();
            minhaconexao.Close();

        }

        public void AlteraProduto(int idProduto, int idMarca, int idTipo, int novoMarca, int novoTipo,
            string nomeProduto, int qtd)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("AlteraProduto", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@idProduto", idProduto);
            consulta.Parameters.AddWithValue("@idMarca", idMarca);
            consulta.Parameters.AddWithValue("@idTipo", idTipo);
            consulta.Parameters.AddWithValue("@novoMarca", novoMarca);
            consulta.Parameters.AddWithValue("@novoTipo", novoTipo);
            consulta.Parameters.AddWithValue("@nomeProduto", nomeProduto);
            consulta.Parameters.AddWithValue("@qtd", qtd);
            consulta.ExecuteNonQuery();
            minhaconexao.Close();

        }

        public void UpdateImagem(int id)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("AlteraImg", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@idProduto", id);

            consulta.ExecuteNonQuery();
            minhaconexao.Close();

        }
        #endregion

        #region  Retorna listas
        public List<Imagem> BuscaImagem(int id) //byte[] para os comentarios
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("BuscarImagemPorIdProduto", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@idProduto", id);
            var retornoDataReader = consulta.ExecuteReader();
            var retorno = addImagens(retornoDataReader);
            minhaconexao.Close();
            return retorno;
        }

        public List<TipoProduto> ListarTipos(int? id)
        {
            var proc = new Procedures();
            var retornoDataReader = proc.ExecutaProc("ListarTipodeProdutos", "@IdTipo", id);
            return addTipos(retornoDataReader);
        }

        public List<MarcaProduto> ListarMarca(int? id)
        {
            var proc = new Procedures();
            var reader = proc.ExecutaProc("OrganizaTipoMarca", "@IdTipoProduto", id);
            var lst = addMarcas(reader);
            return lst;
        }

        public List<MarcaProduto> GetLastMarca()
        {
            var proc = new Procedures();
            var reader = proc.ExecutaProc("SelecionaLastMarca");
            var lst = addMarcasLast(reader);
            return lst;
        }

        public List<Produto> GetLastProduto()
        {
            var proc = new Procedures();
            var reader = proc.ExecutaProc("SelecionaLastProduto");
            var lst = addProdutosLast(reader);
            return lst;
        }

        public List<TipoProduto> GetLastTipo()
        {
            var proc = new Procedures();
            var retornoDataReader = proc.ExecutaProc("SelecionaLastTipo");
            return addTiposLast(retornoDataReader);
        }

        public List<Produto> ListarProdutos()
        {
            var proc = new Procedures();
            var retornoDataReader = proc.ExecutaProc("ListarProdutos");
            var retorno = addProdutos(retornoDataReader);
            return retorno;
        }

        public List<Produto> BuscarProdutos(string nomeProduto, int? idComboTipoProduto, int? idComboMarca,
            decimal? faixaPrecoMin, decimal? faixaPrecoMax)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("buscaProduto", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@nomeProduto", nomeProduto);
            consulta.Parameters.AddWithValue("@idTipoProduto", idComboTipoProduto);
            consulta.Parameters.AddWithValue("@idMarca", idComboMarca);
            consulta.Parameters.AddWithValue("@min", faixaPrecoMin);
            consulta.Parameters.AddWithValue("@max", faixaPrecoMax);
            var retornoDataReader = consulta.ExecuteReader();
            var retorno = addProdutosComMarcaTipo(retornoDataReader);
            minhaconexao.Close();
            return retorno;
        }

        public List<Produto> BuscarProdutosPorId(int id)
        {
            SqlConnection minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
            var consulta = new SqlCommand("BuscarProdutoPorId", minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue("@idProduto", id);
            var retornoDataReader = consulta.ExecuteReader();
            var retorno = addProdutosPorId(retornoDataReader);
            minhaconexao.Close();
            return retorno;
        }

        private List<Produto> addProdutosPorId(SqlDataReader reader)
        {
            var produto = new List<Produto>();
            while (reader.Read())
            {
                var temObjeto = new Produto()
                {
                    NomeProduto = reader["NomeProduto"].ToString(),
                    NomeMarca = reader["NomeMarca"].ToString(),
                    Nometipo = reader["NomeTipoProduto"].ToString(),
                    QuantidadeEstoque = int.Parse(reader["QuantidadeEstoque"].ToString())
                };
                produto.Add(temObjeto);
            }
            reader.Close();
            return produto;
        }

        private List<Imagem> addImagens(SqlDataReader reader)
        {
            var img = new List<Imagem>();
            while (reader.Read())
            {
                var temObjeto = new Imagem()
                {
                    Img = reader["Imagem"].ToString()
                };
                img.Add(temObjeto);
            }
            reader.Close();
            return img;
        }

        private List<TipoProduto> addTipos(SqlDataReader reader)
        {
            var tipo = new List<TipoProduto>();
            while (reader.Read())
            {
                var temObjeto = new TipoProduto(null, null)
                {
                    Id = int.Parse(reader["IdTipoProduto"].ToString()),
                    Nome = reader["NomeTipoProduto"].ToString(),
                    MarcaProduto = new MarcaProduto(int.Parse(reader["IdMarca"].ToString()), null)
                };

                tipo.Add(temObjeto);
            }
            reader.Close();
            return tipo;
        }

        private List<TipoProduto> addTiposLast(SqlDataReader reader)
        {
            var tipo = new List<TipoProduto>();
            while (reader.Read())
            {
                var temObjeto = new TipoProduto(null, null)
                {
                    Id = int.Parse(reader["IdTipoProduto"].ToString())
                };

                tipo.Add(temObjeto);
            }
            reader.Close();
            return tipo;
        }

        private List<MarcaProduto> addMarcas(SqlDataReader reader)
        {
            var marca = new List<MarcaProduto>();
            while (reader.Read())
            {
                var temObjeto = new MarcaProduto(null, null)
                {
                    Id = int.Parse(reader["IdMarca"].ToString()),
                    Nome = reader["NomeMarca"].ToString(),
                };
                marca.Add(temObjeto);
            }
            reader.Close();
            return marca;
        }

        private List<MarcaProduto> addMarcasLast(SqlDataReader reader)
        {
            var marca = new List<MarcaProduto>();
            while (reader.Read())
            {
                var temObjeto = new MarcaProduto(null, null)
                {
                    Id = int.Parse(reader["IdMarca"].ToString())
                };

                marca.Add(temObjeto);
            }
            reader.Close();
            return marca;
        }

        private List<Produto> addProdutosLast(SqlDataReader reader)
        {
            var produto = new List<Produto>();
            while (reader.Read())
            {
                var temObjeto = new Produto()
                {
                    IdProduto = int.Parse(reader["IdProduto"].ToString())
                };
                produto.Add(temObjeto);
            }
            reader.Close();
            return produto;
        }

        private List<Produto> addProdutos(SqlDataReader reader)
        {
            var produto = new List<Produto>();
            while (reader.Read())
            {
                var temObjeto = new Produto()
                {
                    IdProduto = int.Parse(reader["IdProduto"].ToString()),
                    IdTipoProduto = int.Parse(reader["IdTipoProduto"].ToString()),
                    IdMarca = int.Parse(reader["IdMarca"].ToString()),
                    NomeProduto = reader["NomeProduto"].ToString(),
                    ValorCompra = double.Parse(reader["ValorCompra"].ToString()),
                    ValorVenda = double.Parse(reader["ValorVenda"].ToString()),
                    QuantidadeEstoque = int.Parse(reader["QuantidadeEstoque"].ToString())

                };
                produto.Add(temObjeto);
            }
            reader.Close();
            return produto;
        }

        private List<Produto> addProdutosComMarcaTipo(SqlDataReader reader)
        {
            var produto = new List<Produto>();
            while (reader.Read())
            {
                var temObjeto = new Produto()
                {
                    IdProduto = int.Parse(reader["IdProduto"].ToString()),
                    IdMarca = int.Parse(reader["IdMarca"].ToString()),
                    IdTipoProduto = int.Parse(reader["IdTipoProduto"].ToString()),
                    NomeProduto = reader["NomeProduto"].ToString(),
                    NomeMarca = reader["NomeMarca"].ToString(),
                    Nometipo = reader["NomeTipoProduto"].ToString(),
                    QuantidadeEstoque = int.Parse(reader["QuantidadeEstoque"].ToString())

                };
                produto.Add(temObjeto);
            }
            reader.Close();
            return produto;
        }
        #endregion
    }
}