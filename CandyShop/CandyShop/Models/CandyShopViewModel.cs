﻿using System.Linq;
using System.Web.Mvc;

namespace CandyShop.Models
{
    public class CandyShopViewModel
    {
        public CandyShopViewModel()
        {
            ComboTipoProduto = new SelectList(Enumerable.Empty<object>());
            ComboMarca = new SelectList(Enumerable.Empty<object>());
        }

        public SelectList ComboMarca { get; set; }
        public SelectList ComboTipoProduto { get; set; }
    }
}