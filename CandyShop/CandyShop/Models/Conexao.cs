﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace CandyShop
{
    public class Conexao : IDisposable
    {
        private readonly SqlConnection minhaconexao;

        public Conexao()
        {
            minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);


            minhaconexao.Open();
        }

        public void ExecutaComando(string strQuery)
        {
            var cmdComando = new SqlCommand
            {
                CommandText = strQuery,
                CommandType = CommandType.Text,
                Connection = minhaconexao
            };
            cmdComando.ExecuteNonQuery();

        }




        public SqlDataReader ExecutaComandoComRetorno(string strQuery)
        {
            var cmdComando = new SqlCommand(strQuery, minhaconexao);
            return cmdComando.ExecuteReader();
        }


        public void Dispose()
        {
            if (minhaconexao.State == ConnectionState.Open)
                minhaconexao.Close();

        }
    }
}