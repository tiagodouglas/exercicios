﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CandyShop.Models
{
    public class Procedures : IDisposable
    {
        private readonly SqlConnection minhaconexao;

        public Procedures()
        {
            minhaconexao = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexaoComBanco"].ConnectionString);
            minhaconexao.Open();
        }

        public SqlDataReader ExecutaProc(string nomeProc,object nomeVariaveis, object parametros)
        {
            var consulta = new SqlCommand(nomeProc, minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            consulta.Parameters.AddWithValue(nomeVariaveis.ToString(), parametros);

            return consulta.ExecuteReader();
        }

        public SqlDataReader ExecutaProc(string nomeProc)
        {
            var consulta = new SqlCommand(nomeProc, minhaconexao);
            consulta.CommandType = CommandType.StoredProcedure;
            return consulta.ExecuteReader();
        }

        public void Dispose()
        {
            if (minhaconexao.State == ConnectionState.Open)
                minhaconexao.Close();
        }
    }
}