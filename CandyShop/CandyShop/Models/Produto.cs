﻿using System;
using System.ComponentModel;

namespace CandyShop.Models
{
    public class Produto 
    {
        [DisplayName("Id")]
        public int IdProduto { get; set; }

        public int IdTipoProduto { get; set; }

        public int IdMarca { get; set; }

        [DisplayName("Nome")]
        public string NomeProduto { get; set; }

        [DisplayName("Valor de compra")]
        public double ValorCompra { get; set; }

        [DisplayName("Valor de venda")]
        public double ValorVenda { get; set; }

        [DisplayName("Quantidade")]
        public int QuantidadeEstoque { get; set; }
        public DateTime Data { get; set; }
       
        public CandyShopViewModel Combo { get; set; }
         
        public MarcaProduto MarcaProduto { get; set; }

        public TipoProduto TipoProduto { get; set; }

        [DisplayName("Marca")]
        public string NomeMarca { get; set; }
        [DisplayName("Tipo")]
        public string Nometipo{ get; set; }
    }
}