﻿namespace CandyShop
{
    public class TipoProduto
    {
        public TipoProduto(int? id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        public int? Id { get; set; }
        public string Nome { get; set; }

        public MarcaProduto MarcaProduto { get; set; }
    }
}