IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[organizatipop]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[organizatipop]
GO

CREATE PROCEDURE [dbo].[organizatipop]
	@Valor decimal(15,2)
	
	AS

	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Organizar Os produtos pelo tipo e at� um pre�o predefinido
	Autor.............: Tiago Douglas	
	Data..............: 12/04/2017
	Ex................: EXEC [dbo].[organizatipop]5.00
	*/

	BEGIN
			
		SELECT	NomeProduto, 
				ValorVenda, 
				NomeTipoProduto
			FROM Produto p WITH(NOLOCK)
				INNER JOIN TipoProduto tp WITH(NOLOCK)
					ON p.IdTipoProduto = tp.IdTipoProduto
			WHERE ValorVenda < @Valor

	END
GO






IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[listaniver]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[listaniver]
GO

CREATE PROCEDURE [dbo].[listaniver]
	@mesniver INT
	
	AS

	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Lista clientes que fazem anivers�rio em um determinado m�s
	Autor.............: Tiago Douglas	
	Data..............: 13/04/2017
	Ex................: EXEC [dbo].[listaniver]9
	*/

	BEGIN

		SELECT * FROM Cliente WITH(NOLOCK)
			WHERE MONTH(DataNascimento) = @mesniver	

	END
GO









IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[listavendasmes]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[listavendasmes]
GO

CREATE PROCEDURE [dbo].[listavendasmes]
	@mesvendas INT
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Listar as vendas em um determinado m�s
	Autor.............: Tiago Douglas	
	Data..............: 13/04/2017
	Ex................: EXEC [dbo].[listavendasmes]4
	*/

	BEGIN

		SELECT * FROM Venda WITH(NOLOCK)
			WHERE MONTH(DataVenda) = @mesvendas

	END
GO











IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[produtos_naoVend]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[produtos_naoVend]
GO

CREATE PROCEDURE [dbo].[produtos_naoVend]	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Lista todos os produtos n�o vendidos
	Autor.............: Tiago Douglas	
	Data..............: 13/04/2017
	Ex................: EXEC [dbo].[produtos_naoVend]
	*/

	BEGIN
		
		SELECT NomeProduto
			FROM Produto p WITH(NOLOCK)
				WHERE p.IdProduto NOT IN
					(SELECT IdProduto FROM VendaItem vi WITH(NOLOCK)
					 WHERE vi.IdProduto = p.IdProduto)

	END
GO





IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[tipoProdutosMaisVendidos]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[tipoProdutosMaisVendidos]
GO

CREATE PROCEDURE [dbo].[tipoProdutosMaisVendidos]	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Lista os Tipos de Produtos mais vendidos(geral)
	Autor.............: Tiago Douglas	
	Data..............: 13/04/2017
	Ex................: EXEC [dbo].[tipoProdutosMaisVendidos]
	*/

	BEGIN
		
		
		SELECT	NomeProduto,
				NomeTipoProduto,
				Quantidade 
			FROM TipoProduto tp WITH(NOLOCK)
			INNER JOIN Produto p WITH(NOLOCK)
				ON p.IdTipoProduto = tp.IdTipoProduto
			inner join VendaItem vi WITH(NOLOCK)
				ON p.IdProduto = vi.IdProduto
			ORDER BY vi.Quantidade DESC

	END
GO






IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[tipoProdutosMaisVendidosC]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[tipoProdutosMaisVendidosC]
GO

CREATE PROCEDURE [dbo].[tipoProdutosMaisVendidosC]	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Lista os Tipos de Produtos mais vendidos por Cliente
	Autor.............: Tiago Douglas	
	Data..............: 13/04/2017
	Ex................: EXEC [dbo].[tipoProdutosMaisVendidosC]
	*/

	BEGIN
		
		SELECT	NomeCliente,
				NomeTipoProduto, 
				Quantidade      
			FROM TipoProduto tp WITH(NOLOCK)
				INNER JOIN Produto p WITH(NOLOCK)
					ON p.IdTipoProduto = tp.IdTipoProduto
				INNER JOIN VendaItem vi WITH(NOLOCK)
					ON p.IdProduto = vi.IdProduto
				INNER JOIN Venda v WITH(NOLOCK)
					ON vi.IdVenda = v.IdVenda
				INNER JOIN Cliente c WITH(NOLOCK)
					ON c.IdCliente = v.IdCliente
			ORDER BY Quantidade DESC

	END
GO







IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[listaMaiorCompra]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[listaMaiorCompra]
GO

CREATE PROCEDURE [dbo].[listaMaiorCompra]	
	
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Lista as 5 maiores compras 
	Autor.............: Tiago Douglas	
	Data..............: 13/04/2017
	Ex................: EXEC [dbo].[listaMaiorCompra]
	*/

	BEGIN		
		
		SELECT TOP 5	
				vi.IdVenda,	
				c.NomeCliente,		
				NomeProduto,
				ValorVenda,
				Quantidade,
				ValorVenda * Quantidade AS Total
				FROM Produto p WITH(NOLOCK)
					INNER JOIN VendaItem vi WITH(NOLOCK)
						ON p.IdProduto = vi.IdProduto
					INNER JOIN Venda v WITH(NOLOCK)
						ON v.IdVenda = vi.IdVenda
					INNER JOIN Cliente c WITH(NOLOCK)
						ON c.IdCliente = v.IdCliente					
					ORDER BY Total DESC	

	END
GO





IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[listaMenorCompra]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[listaMenorCompra]
GO

CREATE PROCEDURE [dbo].[listaMenorCompra]	
	
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  Lista as 3 menores compras 
	Autor.............: Tiago Douglas	
	Data..............: 16/04/2017
	Ex................: EXEC [dbo].[listaMenorCompra]
	*/

	BEGIN		
		
		SELECT TOP 3	
				vi.IdVenda,	
				c.NomeCliente,		
				NomeProduto,
				ValorVenda,
				Quantidade,
				ValorVenda * Quantidade AS Total
				FROM Produto p WITH(NOLOCK)
					INNER JOIN VendaItem vi WITH(NOLOCK)
						ON p.IdProduto = vi.IdProduto
					INNER JOIN Venda v WITH(NOLOCK)
						ON v.IdVenda = vi.IdVenda
					INNER JOIN Cliente c WITH(NOLOCK)
						ON c.IdCliente = v.IdCliente					
					ORDER BY Total ASC
	END
GO






IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[frequenciasimples]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[frequenciasimples]
GO

CREATE PROCEDURE [dbo].[frequenciasimples]	
	
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........:  select com base nas compras do cliente (frequencia de compras)
	Autor.............: Tiago Douglas	
	Data..............: 17/04/2017
	Ex................: EXEC [dbo].[frequenciasimples]
	*/

	BEGIN
		
		SELECT	c.NomeCliente,			
			p.NomeProduto,
			COUNT(p.NomeProduto) 'Frenqu�ncia(Produtos)'
		FROM Produto p WITH(NOLOCK)
			INNER JOIN VendaItem vi
				ON vi.IdProduto = p.IdProduto
			INNER JOIN Venda v WITH(NOLOCK)
				ON v.IdVenda = vi.IdVenda	   
			INNER JOIN Cliente c WITH(NOLOCK)
				ON c.IdCliente = v.IdCliente
	   GROUP BY p.NomeProduto, c.NomeCliente
	   ORDER BY COUNT(p.NomeProduto) DESC

	END
GO






IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[previsaofa]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[previsaofa]
GO

CREATE PROCEDURE [dbo].[previsaofa]		
	@mes int, @diastrabalhados int
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Fazer a previsao de faturamento durante o m�s
	Autor.............: Tiago Douglas	
	Data..............: 17/04/2017
	Ex................: EXEC [dbo].[previsaofa]2,20
	*/

	BEGIN
		
		SELECT	@mes M�s,
				(SUM(p.ValorVenda) / COUNT(p.ValorVenda)) * @diastrabalhados 'Previs�o de faturamento'		      
		FROM Produto p WITH(NOLOCK)
			INNER JOIN VendaItem vi WITH(NOLOCK)
				ON p.IdProduto = vi.IdProduto
			INNER JOIN Venda v WITH(NOLOCK)
				ON v.IdVenda = vi.IdVenda
		WHERE MONTH(v.DataVenda) = @mes

	END
GO








IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[procuraCliente]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[procuraCliente]
GO

CREATE PROCEDURE [dbo].[procuraCliente]		
	@nome VARCHAR(50)
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Procura nome do Cliente
	Autor.............: Tiago Douglas	
	Data..............: 17/04/2017
	Ex................: EXEC [dbo].[procuraCliente]'Dias'
	*/

	BEGIN

		SELECT * FROM Cliente WITH(NOLOCK)
			WHERE NomeCliente LIKE ('%'+@nome+'%')

	END
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[OrganizaTipoMarca]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[OrganizaTipoMarca]
GO

CREATE PROCEDURE [dbo].[OrganizaTipoMarca]		
	@IdTipoProduto INT = NULL
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Retorna a Marca dos Produtos com base no tipo
	Autor.............: Tiago Douglas	
	Data..............: 26/04/2017
	Ex................: EXEC [dbo].[OrganizaTipoMarca]5
	*/

	BEGIN

	SELECT DISTINCT
				m.IdMarca,
				m.NomeMarca
				FROM Marca m
					LEFT JOIN Produto p 
						ON p.IdMarca = m.IdMarca
					LEFT JOIN TipoProduto tp
						ON p.IdTipoProduto = tp.IdTipoProduto
				WHERE (p.IdTipoProduto = @idtipoProduto OR @idtipoProduto IS NULL)

	END
GO






IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[procuraNomeProduto]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[procuraNomeProduto]
GO

CREATE PROCEDURE [dbo].[procuraNomeProduto]		
	@nome VARCHAR(50)	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Procura nome do Produto
	Autor.............: Tiago Douglas	
	Data..............: 27/04/2017
	Ex................: EXEC [dbo].[procuraNomeProduto]'choco'
	*/

	BEGIN

		SELECT * FROM Produto WITH(NOLOCK)
			WHERE NomeProduto LIKE ('%' + @nome + '%')

	END
GO





IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[ListarTipodeProdutos]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[ListarTipodeProdutos]
GO

CREATE PROCEDURE [dbo].[ListarTipodeProdutos]	
	@IdTipo INT = NULL
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Lista todos os Tipos de produtos
	Autor.............: Tiago Douglas	
	Data..............: 27/04/2017
	Ex................: EXEC [dbo].[ListarTipodeProdutos]102
	*/

	BEGIN

		SELECT DISTINCT
				tp.IdTipoProduto,
				tp.NomeTipoProduto,
				CASE WHEN @IdTipo IS NOT NULL 
					THEN p.IdMarca 
					ELSE 0 END AS IdMarca																																														
		 FROM TipoProduto tp WITH(NOLOCK) 			
			INNER JOIN Produto p ON tp.IdTipoProduto = p.IdTipoProduto			
			WHERE @IdTipo = p.IdProduto OR @IdTipo IS NULL		
		
	END
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[ListarProdutos]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[ListarProdutos]
GO

CREATE PROCEDURE [dbo].[ListarProdutos]	
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Lista todos os produtos
	Autor.............: Tiago Douglas	
	Data..............: 27/04/2017
	Ex................: EXEC [dbo].[ListarProdutos]
	*/

	BEGIN

		SELECT * FROM Produto WITH(NOLOCK)
			
			

	END
GO






IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[MostraValorMinMax]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[MostraValorMinMax]
GO

CREATE PROCEDURE [dbo].[MostraValorMinMax]	
	@min MONEY, @max MONEY
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Mostra os Produtos entre dois valores(minimo e maximo)
	Autor.............: Tiago Douglas	
	Data..............: 28/04/2017
	Ex................: EXEC [dbo].[MostraValorMinMax]2.00,7.00
	*/

	BEGIN

		SELECT * 
			FROM Produto WITH(NOLOCK)
			WHERE ValorVenda > @min AND ValorVenda < @max
		
	END
GO







IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[buscaProduto]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[buscaProduto]
GO

CREATE PROCEDURE [dbo].[buscaProduto]	
	@nomeProduto VARCHAR(50), 
	@idTipoProduto INT = NULL, 
	@idMarca INT = NULL,
	@min MONEY = NULL,
	@max MONEY = NULL
	
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Busca produtos
	Autor.............: Tiago Douglas	
	Data..............: 02/05/2017
	Ex................: EXEC [dbo].[buscaProduto]'',null,5,null,null
	*/

	BEGIN

		SELECT	p.IdProduto,
				P.IdMarca,
				P.IdTipoProduto,
				p.NomeProduto,
				m.NomeMarca,
				tp.NomeTipoProduto,
				p.QuantidadeEstoque				 
			FROM [dbo].[Produto] p WITH(NOLOCK)
				INNER JOIN Marca m ON m.IdMarca = p.IdMarca
				INNER JOIN TipoProduto tp ON tp.IdTipoProduto = p.IdTipoProduto
			WHERE NomeProduto LIKE ('%' + @nomeProduto + '%' ) 
				AND (p.IdTipoProduto = @idTipoProduto OR @idTipoProduto IS NULL OR @idTipoProduto = 0)
				AND (p.IdMarca = @idMarca OR @idMarca IS NULL OR @idMarca = 0)
				AND (ValorVenda >= @min OR @min IS NULL)
				AND (ValorVenda <= @max OR @max IS NULL)
				
		
	END
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[AtualizaQuantidadeEstoque]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[AtualizaQuantidadeEstoque]
GO

CREATE PROCEDURE [dbo].[AtualizaQuantidadeEstoque]	
	@idProduto INT = NULL,
	@qtd INT = NULL
	
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Atualiza a quantidade de produtos em estoque
	Autor.............: Tiago Douglas	
	Data..............: 09/05/2017
	Ex................: EXEC [dbo].[AtualizaQuantidadeEstoque]94,20
	*/

	BEGIN

		UPDATE PRODUTO 			
				SET QuantidadeEstoque =  
					(CASE WHEN QuantidadeEstoque > 0 AND (QuantidadeEstoque - @qtd >= 0) THEN
						QuantidadeEstoque - @qtd END)  
				WHERE IdProduto = @idProduto	
					
	END
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[BuscarProdutoPorId]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[BuscarProdutoPorId]
GO

CREATE PROCEDURE [dbo].[BuscarProdutoPorId]	
	@idProduto INT = NULL 		
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Busca um produto utilizando um id especifico
	Autor.............: Tiago Douglas	
	Data..............: 10/05/2017
	Ex................: EXEC [dbo].[BuscarProdutoPorId]94
	*/

	BEGIN
		
		SELECT	p.NomeProduto, 
				m.NomeMarca,
				tp.NomeTipoProduto,
				p.QuantidadeEstoque
			FROM Produto p  WITH(NOLOCK)
				INNER JOIN Marca m 
					ON m.IdMarca = p.IdMarca
				INNER JOIN TipoProduto tp 
					ON tp.IdTipoProduto = p.IdTipoProduto
			WHERE @idProduto = p.IdProduto 		
							
	END
GO





IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[BuscarImagemPorIdProduto]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[BuscarImagemPorIdProduto]
GO

CREATE PROCEDURE [dbo].[BuscarImagemPorIdProduto]	
	@idProduto INT = NULL
	 		
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Busca uma imagem utilizando o Id do produto como referencia
	Autor.............: Tiago Douglas	
	Data..............: 11/05/2017
	Ex................: EXEC [dbo].[BuscarImagemPorIdProduto]94
	*/

	BEGIN

		SELECT  i.Imagem
			FROM PRODUTO p  WITH(NOLOCK)
			INNER JOIN ImagensProduto i ON i.IdImagem = p.IdProduto
			WHERE @idProduto = p.IdProduto	
										
	END
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[AlteraProduto]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[AlteraProduto]
GO

CREATE PROCEDURE [dbo].[AlteraProduto]	
	@idProduto INT,
	@idMarca INT,
	@idTipo INT,
	@novoMarca INT,
	@novoTipo INT,
	@nomeProduto VARCHAR(50),
	@qtd INT 
			
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Altera dados do produto
	Autor.............: Tiago Douglas	
	Data..............: 12/05/2017
	Ex................: EXEC [dbo].[AlteraProduto]
	*/

	BEGIN
	
		UPDATE Produto SET  NomeProduto = @nomeProduto,
							IdMarca = @novoMarca,
							IdTipoProduto = @novoTipo,
							QuantidadeEstoque = @qtd 	
			WHERE @idProduto = IdProduto
											
	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[AlteraImg]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[AlteraImg]
GO

CREATE PROCEDURE [dbo].[AlteraImg]	
	@idProduto INT
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Altera a imagem
	Autor.............: Tiago Douglas	
	Data..............: 16/05/2017
	Ex................: EXEC [dbo].[AlteraImg]116
	*/
	BEGIN
	
		Update ImagensProduto SET Imagem = '\Content\img\Produtos\' + CAST(@idProduto AS VARCHAR(16)) + '.jpg'
			WHERE IdImagem = @idProduto	
												
	END
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[CadastraProdutoComMarcaTipoExistentes]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[CadastraProdutoComMarcaTipoExistentes]
GO

CREATE PROCEDURE [dbo].[CadastraProdutoComMarcaTipoExistentes]

	@NomeProduto VARCHAR(50),
	@IdTipoProduto INT = NULL,
	@IdMarca INT = NULL,
	@ValorCompra MONEY,
	@ValorVenda MONEY,
	@QuantidadeEstoque INT

			
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Cadastra produtos utilizando um Tipo e Marca j� existente no Banco
	Autor.............: Tiago Douglas	
	Data..............: 22/05/2017
	Ex................: EXEC [dbo].[CadastraProdutoComMarcaTipoExistentes]
	*/

	BEGIN
					
		INSERT INTO Produto(NomeProduto,IdTipoProduto,IdMarca,ValorVenda,ValorCompra,QuantidadeEstoque) 
			VALUES(@NomeProduto,@IdTipoProduto,@IdMarca,@ValorVenda,@ValorCompra, @QuantidadeEstoque)
				
	END
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[CadastraImagem]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[CadastraImagem]
GO

CREATE PROCEDURE [dbo].[CadastraImagem]

	@Imagem	varchar(50)
				
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Cadastra a imagem na posi�ao do produto
	Autor.............: Tiago Douglas	
	Data..............: 23/05/2017
	Ex................: EXEC [dbo].[CadastraImagem]'135.jpg'
	*/

	BEGIN

		INSERT INTO ImagensProduto(Imagem) 
			VALUES('\Content\img\Produtos\' + @Imagem)
		
	END
GO



IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[CadastraMarca]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[CadastraMarca]
GO

CREATE PROCEDURE [dbo].[CadastraMarca]
		
	@NomeMarca VARCHAR(50)

			
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Cadastra Marca do produto 
	Autor.............: Tiago Douglas	
	Data..............: 22/05/2017
	Ex................: EXEC [dbo].[CadastraMarca]"ICEKISS"
	*/

	BEGIN

		INSERT INTO Marca(NomeMarca) 
			VALUES(@NomeMarca)				
							
	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[CadastraTipo]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[CadastraTipo]
GO

CREATE PROCEDURE [dbo].[CadastraTipo]
	
	@NomeTipoProduto VARCHAR(50)
				
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Cadastra Tipo do produto 
	Autor.............: Tiago Douglas	
	Data..............: 22/05/2017
	Ex................: EXEC [dbo].[CadastraTipo]
	*/

	BEGIN
	
		INSERT INTO TipoProduto(NomeTipoProduto) 
				VALUES(@NomeTipoProduto)		
											
	END
GO




IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[SelecionaLastTipo]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[SelecionaLastTipo]
GO

CREATE PROCEDURE [dbo].[SelecionaLastTipo]
	
					
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: retorna a posi��o do ultimo tipo
	Autor.............: Tiago Douglas	
	Data..............: 22/05/2017
	Ex................: EXEC [dbo].[SelecionaLastTipo]
	*/

	BEGIN
	
		SELECT TOP 1 IdTipoProduto 
					FROM TipoProduto WITH(NOLOCK)
					ORDER BY IdTipoProduto DESC
								
	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[SelecionaLastMarca]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[SelecionaLastMarca]
GO

CREATE PROCEDURE [dbo].[SelecionaLastMarca]
	
					
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: retorna a posi��o da ultima marca
	Autor.............: Tiago Douglas	
	Data..............: 22/05/2017
	Ex................: EXEC [dbo].[SelecionaLastMarca]
	*/

	BEGIN
	
		SELECT TOP 1 IdMarca 
					FROM Marca  WITH(NOLOCK) 
					ORDER BY IdMarca DESC								

	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[SelecionaLastProduto]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[SelecionaLastProduto]
GO

CREATE PROCEDURE [dbo].[SelecionaLastProduto]
	
					
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: retorna a posi��o do ultimo produto
	Autor.............: Tiago Douglas	
	Data..............: 22/05/2017
	Ex................: EXEC [dbo].[SelecionaLastProduto]
	*/

	BEGIN
	
		SELECT TOP 1 IdProduto 
					FROM Produto  WITH(NOLOCK) 
					ORDER BY IdProduto DESC								

	END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[ExcluirProduto]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[ExcluirProduto]
GO

CREATE PROCEDURE [dbo].[ExcluirProduto]
	@IdProduto INT
					
	AS
	
	/*
	Documenta�ao
	Arquivo Fonte.....: candyshop.sql
	Objetivo..........: Exclui o produto + imagem do produto
	Autor.............: Tiago Douglas	
	Data..............: 23/05/2017
	Ex................: EXEC [dbo].[ExcluirProduto]
	*/

	BEGIN	
		
		DELETE FROM ImagensProduto
			WHERE IdImagem = @IdProduto	
			
		DELETE FROM Produto
			WHERE IdProduto = @IdProduto 						

	END
GO