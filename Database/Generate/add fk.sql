USE db_CandyShop
ALTER TABLE 
	Venda ADD FOREIGN KEY (IdCliente) REFERENCES Cliente(IdCliente)

ALTER TABLE
	VendaItem ADD FOREIGN KEY (IdVenda) REFERENCES Venda(IdVenda)

ALTER TABLE
	VendaItem ADD FOREIGN KEY (IdProduto) REFERENCES Produto(IdProduto)

ALTER TABLE
	Produto ADD FOREIGN KEY (IdTipoProduto) REFERENCES TipoProduto(IdTipoProduto)


ALTER TABLE 
	Produto ADD FOREIGN KEY (IdMarca) REFERENCES Marca (IdMarca)