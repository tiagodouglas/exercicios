USE db_CandyShop

--Venda
--IdVenda, IdCliente, DataVenda, DataPagamento

--Cliente
--idCliente, NomeCliente, SEXO, DataNascimento, Contato, Ativo, Email

--VendaItem
--IdVendaItem, IdVenda, IdProduto, Quantidade

--Produto
--IdProduto, IdTipoProduto, NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque

--TipoProduto
--IdTipoProduto,NomeTipoProduto

INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (3,1,'TORCIDA JR 50', 0.90, 1.00, 20)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (3,2,'DORIT�O', 13.50 ,15.00, 3)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (4,3,'PASSATEMPO',1.80,2.00,10)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (4,3,'NEGRESCO', 1.80,2.00, 10)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (1,3,'NESCAU', 1.80 ,2.00, 10)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (4,4,'TRAKINAS',1.80 ,2.00, 10)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,5,'BATOM', 0.75,0.80, 20)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,6,'TORTUGUITA',0.75, 0.80, 30)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,3,'CHOCOLATE KIT KAT', 1.80,2.00,15)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,8,'CHOCOLATE STAR',1.35 ,1.50,15)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,8,'CHOCOLATE lAKA', 1.35,1.00,15)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,8,'CHOCOLATE DIAMANTE NEGRO', 0.90, 1.00,15)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,9,'HALLS', 0.90,1.00,20)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,17,'BARRA CEREAIS', 0.90,1.00,20)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,5,'PASTILHA GAROTO', 0.45,0.50,30)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,11,'GOMETS', 0.45,0.50,15)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,17,'PIRULITO', 0.15,0.20,40)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,12,'CHICLETE CLORETS',0.15, 0.20,50)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,19,'PA�OCA', 0.45, 0.50,50)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (5,17,'ESTIKADINHO', 0.45, 0.50,30)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,13,'MORANGUETE', 0.45,0.50, 40)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (2,14,'MMS', 1.80,2.00, 35)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (3,18,'SALGADINHO ELMA CHIPS', 1.80, 2.00, 15)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (6,17,'TORRADINHAS', 1.45, 1.50, 20)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (1,15,'GUARAN�', 4.05 ,4.50, 10)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (1,16,'COCA-COLA',5.40, 6.00, 5)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (1,17,'ENERG�TICO 2L', 5.40,6.00, 5)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (1,17,'ENERG�TICO LATA',5.90, 6.50, 5)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (6,17,'SUCO EM P�', 0.55 ,0.60, 20)
INSERT INTO Produto(IdTipoProduto, IdMarca,NomeProduto, ValorCompra, ValorVenda, QuantidadeEstoque) VALUES (6,17,'GELADINHO', 0.10,0.15, 100)


